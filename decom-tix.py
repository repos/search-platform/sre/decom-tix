#!/usr/bin/env python3

def gen_text(decom_host_range, servers_list):

    FIRST_PART = """
This task will track the #decommission of servers elastic20{}.

With the launch of updates to the decom cookbook, the majority of these steps can be handled by the service owners directly.  The DC Ops team only gets involved once the system has been fully removed from service and powered down by the decommission cookbook.
""".format(decom_host_range)

    SECOND_PART = ""
    SECOND_PART_TEMPLATE = """
`{}`

**Steps for service owner:**

[x] - all system services confirmed offline from production use
[x] - set all icinga checks to maint mode/disabled while reclaim/decommmission takes place. (likely done by script)
[x] - remove system from all lvs/pybal active configuration
[x] - any service group puppet/hiera/dsh config removed
[x] - remove site.pp, replace with role(spare::system) recommended to ensure services offline but not 100% required as long as the decom script is IMMEDIATELY run below.
[x] - login to cumin host and run the decom cookbook: cookbook sre.hosts.decommission <host fqdn> -t <phab task>.  This does: bootloader wipe, host power down, netbox update to decommissioning status, puppet node clean, puppet node deactivate, debmonitor removal, and run homer.
[x] - remove all remaining puppet references and all host entries in the puppet repo
[x] - reassign task from service owner to DC ops team member and site project (ops-sitename) depending on site of server

**End service owner steps / Begin DC-Ops team steps:**

[] - system disks removed (by onsite)
[] - determine system age, under 5 years are reclaimed to spare, over 5 years are decommissioned.
[] - IF DECOM: system unracked and decommissioned (by onsite), update netbox with result and set state to offline
[] - IF DECOM: mgmt dns entries removed.
[] - IF RECLAIM: set netbox state to 'inventory' and hostname to asset tag
"""

    for server in servers_list:
        SECOND_PART += SECOND_PART_TEMPLATE.format(server)


    print(FIRST_PART)
    print(SECOND_PART)

def main():
    decom_host_range = "elastic20[25-36]".split('[')[1].split(']')[0]
    base_host = "elastic20[25-36]".split('[')[0]
    low_host = int(decom_host_range.split('-')[0])
    high_host = int(decom_host_range.split('-')[1])
    servers_list = [ "{}{}".format(base_host,hostname) for hostname in range(low_host, high_host)]
    gen_text(decom_host_range, servers_list)
main()
